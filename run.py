from flywheel_gear_toolkit import GearToolkitContext
import subprocess as sp
import logging
from pathlib import Path

log = logging.getLogger("mpx-to-mat")
EXE = '/flywheel/v0/AlphaOmega/Converter/Mapfile_x64.exe'

def main(context):
    # Get input defined in manifest
    input_file = context.get_input_path("mpx-input")
    args = f"=Z:{input_file}=Matlab=Z:{context.output_dir}="
    args = args.replace('/', '\\')
    exe = ("Z:" + EXE).replace('/','\\')
    with open('./run.bat', 'w', encoding='utf-8') as fp:
        bat = f'"{exe}" {args}'
        log.info(f"Writing bat to run.bat:\n{bat}")
        fp.write(bat)

    cmd = ['xvfb-run', 'wine', 'cmd', '/c', 'run.bat']

    log.info(f"Running command: {' '.join(cmd)}")
    proc = sp.Popen(cmd, stdout=sp.PIPE, stderr=sp.PIPE)
    out, err = proc.communicate()
    log.info(f"{'-'*10}Captured Stdout{'-'*10}")
    for line in out.decode("utf-8", errors="replace").split("\n"):
        log.info(line)
    log.info(f"{'-'*10}Captured Stderr{'-'*10}")
    for line in err.decode("utf-8", errors="replace").split("\n"):
        log.info(line)


if __name__ == '__main__':
    # Initialize Gear Toolkit context
    with GearToolkitContext() as context:
        context.init_logging()
        main(context)
