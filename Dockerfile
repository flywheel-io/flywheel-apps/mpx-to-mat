FROM ubuntu:focal as base

RUN DEBIAN_FRONTEND=noninteractive \
    apt-get update && \
    apt-get install -yq --no-install-recommends \
        wine='5.0-3ubuntu1' \
        xvfb='2:1.20.13-1ubuntu1~20.04.3' \
        python3='3.8.2-0ubuntu2' \
        python3-pip="20.0.2-5ubuntu1.6"

ENV FLYWHEEL=/flywheel/v0
WORKDIR $FLYWHEEL

COPY . .

RUN pip install -r requirements.txt

ENTRYPOINT ["./run.py"]
